import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TodosService {
    rootUrl: string = 'http://localhost:3001/tasks';
    constructor(private http: Http) { }
    
    getAllTodos() {
        return this.http.get(this.rootUrl)
            .map((response: Response) => response.json());
    }

    getTodo(id: number) {
        return this.http.get(this.rootUrl + '/' + id)
            .map((response: Response) => response.json());
    }

    addTodo(todo: any) {
        return this.http.post(this.rootUrl, todo)
            .map((response: Response) => response.json());
    }

    udpateTodo(id: number, todo: any) {
        return this.http.put(this.rootUrl + '/' + id, todo)
            .map((response: Response) => response.json());
    }

    deleteTodo(id: number) {
        return this.http.delete(this.rootUrl + '/' + id)
            .map((response: Response) => response.json());
    }
}
