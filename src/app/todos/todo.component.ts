import { Component, OnInit } from '@angular/core';
import { TodosService } from './todo.service';

@Component({
    moduleId: module.id,
    selector: 'todos',
    templateUrl: 'todo.component.html'
})

export class TodosComponent implements OnInit {
    public todos: Array<Object> = [];
    public newTodo: string = '';

    constructor(private todosService: TodosService) { }

    ngOnInit() { 
        this.getAllTodos();
    }

    getAllTodos() {
        this.todosService.getAllTodos()
            .subscribe(res => this.todos = res);
    }

    deleteTodo(id) {
        this.todosService.deleteTodo(id)
            .subscribe(res => {
                this.getAllTodos();
            });
    }

    addTodo() {
        this.todosService.addTodo({name: this.newTodo})
            .subscribe(res => {
                this.newTodo = '';
                this.getAllTodos();
            });
    }
}
